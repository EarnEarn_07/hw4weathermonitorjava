# Weather Monitor in Android

This is Android Studio project, you might need to import into your Android Studio.

# Your work
* Fork my repo
* Subscribe to OpenWeatherMap API https://openweathermap.org/current
* Update the API URL in DisplayActivity
* Update method updateFromDownload, so it can process JSON and put into corresponding textview. At least, you need Temperature, Humidity, Pressure, Wind Speed/Direction

# Submission
* Do a pull request into my repo